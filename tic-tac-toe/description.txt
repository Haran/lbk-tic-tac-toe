��������� � �++ � ���� ���� �� �������� ��������, ������ ������ � GIT, 
�����  ����� ���� ���������,  ���� � �����-�� ������  ������ ��������.
��� � �������� ��������������� ����� ������� 38b3c42, ��� ��-�� �����
�������� ������ ����������  ����������� ������.  ������������  ������
����� ������� IDE � ��������� ���������� ������� � code folding'�.


����������� ��������� �� Bitbucket, ���� ����������� - ������ ������ �
������, ����� ����� ����������� ��� �������.  ��� ��� ������ ��������� 
� ������������.


�������� �������� ����� ������ ������� ��� �������� ���������. �������
����� ����������,  ��� ��� �������,  �� ������  ������� ����� ��������
��� ����, ���� �� ��������� ��������� ����� ����� ������ ����������.


� �������� ������ � �����������  ����, Dev-cpp ��������  �������������
����, ��� ����� ���������� - ��� ��������� �����,  ��� code folding'�,
�� ����� ��������� �������� ���� �������������� ����.  � �����, ������
���� ���������� � ����� ����� � ����� ��� �� NetBeans + MinGW.  �� ���
������ ��������� ������ Run, �� � ������������� ���������� ����������.
��� �������,  � �������� ������� �����  ���������� ��� ���� � NetBeans
������ �� ��� �� �������� � ��� ���������� ���� � JetBrains.

