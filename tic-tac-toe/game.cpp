#include <iostream>
#include "windows.h"

/**
 * ���� ��������-������
 * 
 * ������������� � windows ��-�� ��������� �������
 * �-�� system('cls'), �� ��� �� �������� ��������
 * ����� �������� \n ����� ��� ����-���������.
 */
using namespace std;

/**
 * ����������� ��������� � ������ ����
 */
#include "attack.h"
#include "defense.h"

// �������� ������� ���������-�������
char square[10] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};

/**
 * ����� ����
 * int 1: ������ ����������
 * int 2: ������ ���������
 */
int mode;

/**
 * Echo ��� ������ ����
 * @return void
 */
void printMenu()
{
    cout << "�������� ��������\n" << endl;
    cout << "1. ���� ������ ����������\n"
         << "2. ���� � ������ ���������\n"
         << "0. �����\n" << endl;
    cout << ">>> ";
}


/**
 * ������� ������� �����
 * @return void
 */
void clearBoard()
{
    for( int i=1; i<10; i++) {
        square[i] = ' ';
    }
}


/**
 * �������� �� ��������
 * @return void
 */
void delay()
{
     cout << "�����...\n";
     srand (time(NULL));
     Sleep((700 + (rand() % (int)(2100 - 800 + 1))));
}


/**
 * ������ �����
 * @return void
 */
void drawBoard()
{
     
    system("cls");
    
    switch(mode) {
        case 1:
            cout << "===============================================\n"
                 << "                                 | ������ �����\n"
                 << "������ ������ ����������         |    1 2 3    \n"
                 << "��������� ������ ������ �������� |    4 5 6    \n"
                 << "                                 |    7 8 9    \n"
                 << "===============================================\n"
            ;
        break;
        case 2:
            cout << "==============================================\n"
                 << "��������� ��� ����� ������      | ������ �����\n"
                 << "������ ����� ������ ������      |    1 2 3    \n"
                 << "����������, � ������ - �������� |    4 5 6    \n"
                 << "                                |    7 8 9    \n"
                 << "==============================================\n"
            ;
        break;
    }
    
    cout << endl;
    cout << "  " << "     |     |     \n";
	cout << "  " << "  " << square[1] << "  |  " << square[2] << "  |  " << square[3] << endl;
	cout << "  " << "_____|_____|_____\n";
	cout << "  " << "     |     |     \n";
	cout << "  " << "  " << square[4] << "  |  " << square[5] << "  |  " << square[6] << endl;
	cout << "  " << "_____|_____|_____\n";
	cout << "  " << "     |     |     \n";
	cout << "  " << "  " << square[7] << "  |  " << square[8] << "  |  " << square[9] << endl;
	cout << "  " << "     |     |     \n\n";
	
}


/**
 * ������� ���� ������������, ��������� ���� � ������� ����� ��������
 * @return integer
 */
int promptMenu()
{
    
    int input;
    printMenu(); 

    cin >> input;
    system("cls"); // ������� �����
    
    while (!cin) {
        cin.clear();
        cin.ignore(256,'\n');
        system("cls");
        printMenu();
        cin >> input;
    }
    
    return input;
    
}


/**
 * ������������� ���� ������
 * @param player integer ����� ������
 * @return void
 */
void makeMove(int player)
{

    int choice;
    char mark;
    
    cout << "����� " << player << ", ������� ����� ����: ";
    cin >> choice;
    
    while (!cin || square[choice]!=' ') {
        cout << "������. ������� ���������� ����� ����: ";
        cin.clear();
        cin.ignore(256,'\n');
        cin >> choice;
    }

    mark = (player == 1) ? 'X' : 'O';
    square[choice] = mark;
    drawBoard();

}


/**
 * ��������, ���������� �� ������ � ����
 * 1  : ���-�� �������
 * 0  : ���� ������������
 * -1 : �����
 *
 * @return integer
 */
int isGameOver()
{

    // ��������� ���������
    if(
       (square[1] != ' ' && ( square[1] == square[5] && square[5] == square[9] )) ||
       (square[3] != ' ' && ( square[3] == square[5] && square[5] == square[7] ))
    ) return 1;

    // ����������� � ���������
    for(int i = 0; i < 3; i++) {
        
        // ����������� ���������
        if ( square[i*3+1] != ' ' && ( square[i*3+1] == square[i*3+2] && square[i*3+2] == square[i*3+3] ) )
        return 1;
        
        // ��������� ���������
        if ( square[i+1] != ' ' && ( square[i+1] == square[i+4] && square[i+4] == square[i+7] ) )
        return 1;
        
    }
    
    // ���� ���� ���� ���� �� ������ ������ - ���� ������������
    for( int j = 1; j < 10; j++  ) {
        if( square[j] == ' ' ) {
            return 0;
        }
    }
    
    // ����� - �����
    return -1;
    
}


/**
 * ������ ��� � ��������� ������
 * @param field integer ����� ����
 * @return void
 */
void writeTo(int field)
{
    square[field] = 'O';
    delay();
    drawBoard();
}


/**
 * ��� ����
 * TODO: ��������. ����������� ������.
 * @param move integer ���������� ����� ����
 * @return void
 */
void botMove(int move)
{
    
    // ������ ��� ���������� ����
    int cell = 0;
    
    // �����������, ������� �� ��� ���
    // �� ����� ��������� ������ ������� ����
    bool attack = false;
    if( move % 2 == 1 ) attack = true;
    
    // ������ ����� ������ ��������� ������ ����� ����
    if( move == 1 ) {
        writeTo(5);
        return;
    }
    
    // ���� ��� ����� ������ - �������� ����� ����,
    // ���� ����� ���� ����� - �������� ����.
    if( move == 2 ) {
        if( square[5] == ' ' ) writeTo(5);
        else writeTo(1);
        return;
    }
    
    if( attack ) {
        cell = performAttack(move);
    }
    else {
        cell = performDefense(move);
    }
    
    // ���� ���� �������� ������ ������ ��� ������������ ����
    if( cell != 0 && square[cell] == ' ' ) {
        writeTo(cell);
        return;
    }
    
    // ���� �������� 0 - ��� �� ������ ������ ������
    for( int i = 1; i< 10; i++) {

        if( square[i] == ' ' ) {
            writeTo(i);
            return;
        }

    }
    
}


/**
 * ����� ��������� � ������
 * @param player integer ����� ������
 * @return void
 */
void winAnnounce(int player)
{
     switch(player) {
         case 0:
             cout << "��������� �������!\n";
             break;
         default:
             cout << "����� " << player << " �������!\n";
             break;
     }
}


/**
 * ���� ������ ����������
 * @param game integer ���������� ����� ��������� ������
 * @return void
 */
void playVsAi(int game)
{

    int player;
    
    clearBoard();
    drawBoard();

    for (int move = 1; move < 10; move++) { 
        
        // ����� ��� ��� (1 ��� 0)
        player = abs((move % 2) - (game % 2));
        
        if( player == 1 ) makeMove(player);
        else botMove(move);
          
        // �������� �� ������ ����� 4 ����
        if( move > 4 && move < 10 ) {
            
            if( isGameOver() == 1 ) {
                winAnnounce(player);
                break;
            }            
            if( isGameOver() == -1 ) {
                cout << "�����!\n";
                break;
            }

        }

    }

}


/**
 * ���� ������ ������� ������
 * @return void
 */
void playVsHuman()
{
     
    int  player;
    
    clearBoard();
    drawBoard();
    
    for (int move = 1; move < 10; move++) { 
    
        player = (move % 2) ? 1 : 2;
        makeMove(player);

        // �������� �� ������ ����� 4 ����
        if( move > 4 && move < 10 ) {
            
            if( isGameOver() == 1 ) {
                winAnnounce(player);
                break;
            }
            if( isGameOver() == -1 ) {
                cout << "�����!\n";
                break;
            }

        }

    }

}

/**
 * MAIN
 */
int main()
{

    setlocale(LC_ALL, "Russian");
    mode = promptMenu();
    
    char play = 'y';
    int  game = 0;
    bool yn   = false;

    while(play == 'y') {

        // ������� ���
        game++;
        
        switch (mode) {
            // vs AI
            case 1:
                playVsAi(game);
            break;
            
            // Player vs Player
            case 2:
                 playVsHuman();
            break;
            
            case 0:
            
            // �����
            default:
                exit(0);
            break;
        }
                
        cout << "��� �����? (y/n): ";
        cin >> play;
        
        // �� ����, ��� ��� ������� � while ��������...
        if( play == 'n' || play == 'y'){
            yn = true;
        }
        
        // ������������ ���� ������
        while ( !cin || !yn ) {
            
            cout << "��� �����? (y/n): ";
            cin.clear();
            cin.ignore(256,'\n');
            cin >> play;
            
            // �� ����, ��� ��� ������� � while ��������...
            if( play == 'n' || play == 'y') {
                yn = true;
            }
            
        }
        
    }
   
    system("pause");
    return 0;
    
}
